import { Component } from '@angular/core';

@Component({
    selector: 'user',
    template: `
<h1>Hello {{name}}</h1>
  <p><strong>Email: </strong> {{email}}</p>
  <p><strong>Address: </strong>{{address.street}} {{address.city}} , {{address.state}}</p>
  <button (click)="toggleHobbies()">{{showHobbies ? "Hide Hobbies" : "Show Hobbies" }}</button>
  <div *ngIf="showHobbies"> 
  
  
  <h3>Hobbies</h3>
  <ul>
  <li *ngFor="let hobby of hobbies; let i = index "> <!--(6) i for delete hobby-->
  {{hobby}}
  <!--(6)Input and Binding For Hobbies( After ng model) deleteHobby() down"-->
  
  <button (click)="deleteHobby(i)">X</button>
  
   <!--(6)Input and Binding For Hobbies-->
  </li>
  </ul>
   
  
  
  <!--(5)Input and Binding For Hobbies( After ng model) addHobby() down"-->
  <form (submit)="addHobby(hobby.value)" >
  <label>Add Hobby: </label><br />
  <input type="text" #hobby /> <br/>
  </form>
  <!--(5)Input and Binding For Hobbies-->
</div>
<hr/>
<!--(4). Input and Binding ([(ngModel)]"-->

<h3>Edit User</h3>
  <form>
  <label>Name: </label><br />
  <input type="text" name="name" [(ngModel)]="name"/> <br/>
  <label>Email: </label><br />
  <input type="text" name="email" [(ngModel)]="email"/> <br/>
  <label>Street: </label><br />
  <input type="text" name="address.street" [(ngModel)]="address.street"/> <br/>
  <label>City: </label><br />
  <input type="text" name="address.city" [(ngModel)]="address.city"/> <br/>
  <label>State: </label><br />
  <input type="text" name="address.state" [(ngModel)]="address.state"/> <br/>
</form>
<!--Input and Binding (4)-->
`,
})
export class UserComponent {
    name : string;
    email : string;
    address: address;
    hobbies: string[];
    showHobbies: boolean;

    constructor() {
        this.name = 'Hasan';
        this.email = 'john@gmail.com';
        this.address = {
            street: '12 Main st',
            city: 'dhaka',
            state: 'Dhaka'


    }

      this.hobbies =['Music' , 'Movies', 'sports'];
        this.showHobbies = false;
    }
    toggleHobbies(){
        if(this.showHobbies ==true){
            this.showHobbies = false;
        } else {
            this.showHobbies = true;
        }
    }

    addHobby(hobby){
        this.hobbies.push(hobby);
    }
    deleteHobby(i){
        this.hobbies.splice(i, 1);
    }

}

interface address{
    street: string;
    city :string;
    state:string;

}

